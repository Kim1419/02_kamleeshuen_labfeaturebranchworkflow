﻿using System;

namespace _KamLeeShuen_LabFeature
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int number = 0;
            int sum = 0;

            Console.WriteLine("Please enter a number: ");
            number = Convert.ToInt32(Console.ReadLine());
            for (int i = 1;i<=number;i++)
            {
                Console.WriteLine(i);
                sum += i;
            }
            Console.WriteLine("The sum is:" + sum);
        }
    }
}
